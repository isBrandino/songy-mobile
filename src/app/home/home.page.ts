import { Component, ViewChild } from '@angular/core';
import { IonRange } from '@ionic/angular';
import { Howl, Howler } from 'howler';
export interface Audio {
  name: string;
  path: string;
}

function fileName(path: string){
  path = path.replace(/^.*[\\\/]/, '') ;
  path = path.replace(/_/g, ' ');
  return path;
}

function getContent() {
  return document.querySelector('ion-content');
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {


  active: Audio = {name : null, path: null};
  player: Howl = null;
  playing = false;
  progress = 0;
  defaultvolume = 0.5;
  @ViewChild('range', {static: false}) range: IonRange;
  @ViewChild('vol', {static: false}) vol: IonRange;

  playlist: Audio[] = [
    {
      path: './../../assets/audio/wulf_bass_1542_rest.mp3',
      name: fileName('./../../assets/audio/wulf_bass_1542_rest')
    },
    {
      path: './../../assets/audio/alcoholic_basstone.mp3',
      name: fileName('./../../assets/audio/alcoholic_basstone')
    },
    {
      name: 'grodd',
      path: './../../assets/audio/grod_samples.mp3',
    },
    {
      name: 'Bababooey moombahton remix DJ Melvio',
      path: './../../assets/audio/Bababooey_moombahton_remix_DJ_Melvio.mp3'
    },
    {
      name: 'The Vampiric Tyrant',
      path: './../../assets/audio/The_Vampiric_Tyrant.mp3'
    },
    {
      name: 'u even flopp bro',
      path: './../../assets/audio/u_even_flopp_bro.mp3'
    },
    {
      name: 'I',
      path: './../../assets/audio/I.mp3'
    },
    {
      name: 'II',
      path: './../../assets/audio/II.mp3'
    },
    {
      name: 'III',
      path: './../../assets/audio/III.mp3'
    },
  ]

  constructor() { }

  start(audio: Audio) {
    if(this.player){
      this.player.stop();
    }
    Howler.autoUnlock = false;
    this.player = new Howl({
      src: [audio.path],
      html5: true,
      onplay: () => {
        this.playing = true;
        this.active = audio;
        this.updateProgress();
      },
      
      onend: () => {

      }
      
    });
    this.player.play();
  }

  togglePlay(pause) {
    
    this.playing == !pause;

    if (!pause) {

      this.player.pause();
      this.playing = false;

    } else {

      this.player.play();
      this.playing = true;
    }
  }

  next() {
    console.log("add")
    let index = this.playlist.indexOf(this.active);
    if(index != this.playlist.length - 1){
      this.start(this.playlist[index + 1])
    } else {
      this.start(this.playlist[0])
    }
  }

  repeat() {
    this.player.seek(0);
  }


  prev() {
    console.log("cats")
    let index = this.playlist.indexOf(this.active);
    if(index > 0){
      this.start(this.playlist[index - 1])
    } else {
      this.start(this.playlist[this.playlist.length - 1])
    }
  }

  seek() {
    let move = +this.range.value;
    let duration = this.player.duration();
    this.player.seek(duration * (move / 100))
  }


  updateProgress(){
    let seek = this.player.seek();
    this.progress = (seek / this.player.duration()) * 100 || 0;

    setTimeout(() => {
      this.updateProgress();
    }, 750)

  }

  volume(){
    let mute = (+this.vol.value);
    Howler.volume(mute);
  }
  search(){
    const searchbar = document.querySelector('ion-searchbar');
    const items = this.playlist;

  }

  scrollToBottom() {
    getContent().scrollToBottom(500);
  }
  
  scrollToTop() {
    getContent().scrollToTop(500);
  }
}
