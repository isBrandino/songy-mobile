import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'audioo',
  webDir: 'www',
  bundledWebRuntime: false,
};

export default config;
